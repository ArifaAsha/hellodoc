SET PASSWORD FOR 'root'@'localhost' = PASSWORD ('root');

CREATE TABLE PATIENT
(
    ID       BIGINT AUTO_INCREMENT,
    FIRST_NAME VARCHAR(30) NOT NULL,
    LAST_NAME  VARCHAR(30) NOT NULL,
    EMAIL      VARCHAR(30) NOT NULL UNIQUE,
    PHONE_NO   VARCHAR(11) NOT NULL,
    PASSWORD   VARCHAR(20) NOT NULL,
    GENDER     VARCHAR(10) NOT NULL,
    DOB        DATE    NOT NULL,
    CONSTRAINT PRIMARY KEY (ID)
);

CREATE TABLE DOCTOR
(
    ID       BIGINT AUTO_INCREMENT,
    BMDC_REG  VARCHAR(30) NOT NULL,
    SPECIALIZATION VARCHAR(50) NOT NULL,
    FIRST_NAME VARCHAR(30) NOT NULL,
    LAST_NAME  VARCHAR(30) NOT NULL,
    EMAIL      VARCHAR(30) NOT NULL UNIQUE,
    PHONE_NO   VARCHAR(11) NOT NULL,
    PASSWORD   VARCHAR(20) NOT NULL,
    GENDER     VARCHAR(10) NOT NULL,

    CONSTRAINT PRIMARY KEY (ID)
);

CREATE TABLE CHAMBER
(
    ID BIGINT AUTO_INCREMENT PRIMARY KEY,
    DOCTOR_ID BIGINT NOT NULL,
    NAME VARCHAR(50) NOT NULL,
    LOCATION VARCHAR(255) NOT NULL,
    OFF_DAYS VARCHAR(255),
    START_FROM TIME NOT NULL,
    END_AT TIME NOT NULL,

-- TODO: ADD COLUMN VISIT

    CONSTRAINT CHAMBER_FK_DOC_HAS FOREIGN KEY(DOCTOR_ID) REFERENCES DOCTOR(ID)
);