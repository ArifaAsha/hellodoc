<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "hello_doctor";

function getConnection()
{
    global $servername;
    global $username;
    global $password;
    global $dbname;

    // Create connection
    $conn = mysqli_connect($servername, $username, $password, $dbname);

    // Check connection
    if (!$conn)
        die("Connection failed: " . mysqli_connect_error());

    return $conn;
}

?>