INSERT INTO doctor(BMDC_REG, SPECIALIZATION, FIRST_NAME, LAST_NAME, EMAIL, PHONE_NO, PASSWORD, GENDER) values
("D-11", "Cardiologist", "John", "Smith", "dr@mail.com", "0123892", "d", "Male" ),
("D-13", "Endocrinologists", "Bob", "Marley", "bob@mail.com", "01113892", "a", "Male"),
("D-14", "Cardiologist", "Jane", "Doe", "jane@mail.com", "01222002", "a", "Female" );


INSERT INTO CHAMBER(DOCTOR_ID, NAME, LOCATION, OFF_DAYS, START_FROM, END_AT) VALUES
(1, "Ibn-Sina", "Sector-13, Uttara, Dhaka", "FRIDAY", "09:00:00", "19:00:00"),
(1, "Square", "Sector-17, Uttara, Dhaka", "SATURDAY", "10:00:00", "15:30:00"),
(1, "Popular", "Sector-13, Uttara, Dhaka", "THURSDAY", "11:00:00", "17:30:00"),
(1, "Prescription Point Ltd.", "Banani", "SUNDAY", "10:00:00","18:30:00");
