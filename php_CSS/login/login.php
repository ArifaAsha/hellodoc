<!DOCTYPE html>

<html>
    <head>
        <title>Log in</title>
        <link rel="stylesheet" href="../../contents/fontawesome-free-5.13.0-web/css/all.min.css">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="../../contents/bootstrap/css/bootstrap.min.css">
        <script src="../../contents/jQuery/jquery.min.js"></script>
        <script src="../../contents/popper.min.js"></script>
        <script src="../../contents/bootstrap/js/bootstrap.min.js"></script>

        <link rel="stylesheet" href="../navbar.css">
        <link rel="stylesheet" type="text/css" href="login.css" >

    </head>

    <body>
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <a class="fas fa-user-md navbar-brand" href="#">Hello Doctor</a>

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="../index/index.php">Home</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="../register/registration.php">Register</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="login.php">Log in</a>
                </li>
            </ul>
        </nav>
        <br><br>

    <h1>Log in</h1>
    <br/>
    <div class="registration-form-container">
        <form id="registration-form" method="post" action="./verifyLogin.php">

            <label>Email<span class="required">*</span></label><br>
            <input class="email" type="email" name="email" placeholder="email" id="email"><br><br>

            <label>Password<span class="required">*</span></label><br>
            <input class="password" type="password" name="password" placeholder="Password" id="name"><br><br>

            <div class="text-center">
                <button class="btn btn-primary" type="submit" id="submit" name="submit">Log in</button>
            </div>
            <span style="display: none; color: red" class="error" id="login-failed"></span>
        </form>
    </div>
    </body>
</html>

