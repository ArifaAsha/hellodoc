<?php
if(!isset($_SESSION))
    session_start();
?>
<!doctype HTML>
<html>
<head>
    <title>Hello Doctor</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../../contents/fontawesome-free-5.13.0-web/css/all.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../../contents/bootstrap/css/bootstrap.min.css">
    <script src="../../contents/jQuery/jquery.min.js"></script>
    <script src="../../contents/popper.min.js"></script>
    <script src="../../contents/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="index.css">


</head>

<body>
    <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
        <a class="fas fa-user-md navbar-brand" href="#">Hello Doctor</a>

        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Home</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="../register/registration.php">Register</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="../login/login.php">Log in</a>
            </li>
        </ul>
    </nav>
    <br><br>
</body>
</html>

