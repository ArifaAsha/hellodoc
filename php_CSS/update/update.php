<?php
session_start();

if ($_SESSION['isloggedin'] == true) {
    ///show the home page

    ///way 2: to show frontend code

    ?>


    <?php
        if (isset($_GET['ID']))
            $update_id = $_GET['ID'];

        try {
            $conn = new PDO("mysql:host=localhost:3306;dbname=hello_doctor", "root", "");


            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $ex) {
    ?>

        <script>
            alert("Database connection error");
        </script>
        <?php
    }

    $mysqlquery = "select * from chamber where ID=$update_id";
//    print_r($mysqlquery);

    $result = $conn->query($mysqlquery); ///$result object
    ///no of rows, $result->rowCount();

    if ($result->rowCount() == 1) {
        ///reading the whole table
        $table = $result->fetchAll();

        $row = $table[0];

        ?>
        <html>
            <head>
                <meta charset="utf-8">
                <title>Update</title>

                <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="stylesheet" href="../../contents/bootstrap/css/bootstrap.min.css">
                <link rel="stylesheet" href="../../contents/fontawesome-free-5.13.0-web/css/all.min.css">
                <link rel="stylesheet" type="text/css" href="update.css" >
<!--                <link rel="stylesheet" href="../home.css">-->
<!--                <link rel="stylesheet" type="text/css" href="../navbar.css">-->

            </head>
            <body>

            <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
                <a class="fas fa-user-md navbar-brand" href="../home.php">Hello Doctor</a>

                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="../home.php">Home</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="../logout/logout.php">Log out</a>
                    </li>
                </ul>
            </nav>
            <br><br>

            <h1>Update Chamber Info</h1>

            <div class="container">
                <div class="form_container">
                    <form id="form-class" action="./updateData.php" method="post">
                        <label>Chamber Name</label><br>
                        <input type="text" id="name" name="name" value="<?php echo $row['NAME'] ?>">
                        <br/>

                        <label>Location</label><br>
                        <input type="text" id="location" name="location" value="<?php echo $row['LOCATION'] ?>">
                        <br/>

                        <label>Off Days</label><br>
                        <input type="text" id="off_days" name="off_days" value="<?php echo $row['OFF_DAYS'] ?>">
                        <br/>

                        <label>Start from</label><br>
                        <input type="text" id="start_form" name="start_from" value="<?php echo $row['START_FROM'] ?>">
                        <br/>

                        <label>End at</label><br>
                        <input type="text" id="end_at" name="end_at" value="<?php echo $row['END_AT'] ?>">
                        <br/>

                        <input type="hidden" id="update_id" name="update_id" value="<?php echo $update_id ?>">
                        <br/>

                        <div class="text-center">
                            <button class="btn btn-primary" type="submit" id="submit" name="submit">Update</button>
                        </div>
                    </form>
                </div>
            </div>
            </body>
        </html>

        <?php
//        echo "$update_id";

    }
    ?>


    <?php

} else {
    ?>
    <script>location.assign('../login/login.php');</script>

    <?php
}
?>