<?php
session_start();

if($_SESSION['isloggedin']==true){
    ///show the home page
    ///way 2: to show frontend code
    ?>
    <!--you page code-->
    <!DOCTYPE html>

    <html>

    <head>
        <meta charset="utf-8">
        <title>Log In</title>
        <link rel="stylesheet" href="./home.css">

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="../contents/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="../contents/fontawesome-free-5.13.0-web/css/all.min.css">
    </head>

    <body>

        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <a class="fas fa-user-md navbar-brand" href="#">Hello Doctor</a>

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="">Update Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="./logout/logout.php">Log out</a>
                </li>
            </ul>
        </nav>
        <br><br>



        <div id="homebody">
            <!--product show -->
            <h1 class="headline">CHAMBERS</h1>
            <table>
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Name</th>
                        <th>Location</th>
                        <th>Off Days</th>
                        <th>Start From</th>
                        <th>End At</th>
                        <th>Operations</th>
                    </tr>
                </thead>

                <tbody>
                <?php

                try{
                    $conn=new PDO("mysql:host=localhost:3306;dbname=hello_doctor","root","");


                    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                }
                catch(PDOException $ex){
                    ?>
                    <!--                                        outside php -->
                    <script>
                        alert("Database connection error");
                    </script>
                    <?php
                }

                $mysqlquery="SELECT ID, NAME, LOCATION, OFF_DAYS, START_FROM, END_AT FROM CHAMBER";

                $result=$conn->query($mysqlquery); ///$result object
                ///no of rows, $result->rowCount();

                ///reading the whole table
                $table=$result->fetchAll();


                ////print_r($table);


                ///processing
                for($i=0;$i<count($table);$i++){
                    ///$row 1D array
                    $row=$table[$i];
                    ?>

                    <tr>
                        <td><?php echo $i+1 ?></td>
                        <td><?php echo $row['NAME'] ?></td>
                        <td><?php echo $row['LOCATION'] ?></td>
                        <td><?php echo $row['OFF_DAYS'] ?></td>
                        <td><?php echo $row['START_FROM'] ?></td>
                        <td><?php echo $row['END_AT'] ?></td>
                        <td>
                            <input type="button" id="updateButton" class="buttonUpdate" value="Update"  onclick="updatefn(<?php echo $row['ID'] ?>);">
                            <input type="button" value="Delete" class="buttonDelete" onclick="deletefn(<?php echo $row['ID'] ?>);">
                        </td>
                    </tr>
    <!--                --><?php //echo $row['ID']?>
                    <?php
                }
                ?>
                </tbody>
            </table>

        </div>
<!--        <a href="logout.php">Click to Logout</a>-->


        <script>
            function deletefn(del_chamber_id){
                var choice=confirm("Do you want to delete this?");
                if(choice){
                    location.assign("./delete/delete.php?d_id="+del_chamber_id);
                }
            }

            function updatefn(upd_chamber_id){
                location.assign("./update/update.php?ID="+upd_chamber_id);
            }
        </script>
        </body>
        </html>
        <?php

} else{
        ?>
        <script>location.assign('./login/login.php');</script>

        <?php
    }
    ?>



