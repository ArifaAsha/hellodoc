<!doctype HTML>
<html>
<head>
    <title>Doctor Registration Form</title>
    <meta charset="utf-8">
    <link rel="stylesheet" href="../../contents/fontawesome-free-5.13.0-web/css/all.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="../../contents/bootstrap/css/bootstrap.min.css">
    <script src="../../contents/jQuery/jquery.min.js"></script>
    <script src="../../contents/popper.min.js"></script>
    <script src="../../contents/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="./registration.css" >
</head>

<body>

<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <a class="fas fa-user-md navbar-brand" href="#">Hello Doctor</a>

    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="../index/index.php">Home</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="../login/login.php">Log in</a>
        </li>

    </ul>
</nav>
<br><br>

<div class="container">
    <h1>Create your account</h1>
    <div class="registration-form-container">
        <form id="registration-form" method="post" action="./registration.php">
            <label>BMDC Reg. No<span class="required">*</span></label><br>
            <input type="text" name="bmdc" placeholder="BMDC Reg. No" id="bmdc_no"><br><br>

            <label for="specialization">Specialization: </label><br>
            <select class="browser-default custom-select" id="specialization" name="specialization">
                <option value="selectOption">--select an option</option>
                <option value="Cardiologists">Cardiologists</option>
                <option value="Endocrinologists">Endocrinologists</option>
                <option value="Gastroenterologists">Gastroenterologists</option>
                <option value="obs">Obstetrician/gynecologists</option>
                <option value="Allergists">Allergists</option>
                <option value="Ophthalmologists">Ophthalmologists</option>
                <option value="Pediatricians">Pediatricians</option>
                <option value="Nephrologists">Nephrologists</option>
                <option value="Urologists">Urologists</option>
                <option value="Pulmonologists">Pulmonologists</option>
                <option value="Otolaryngologists">Otolaryngologists(ENT)</option>
                <option value="Neurologists">Neurologists</option>
                <option value="Psychiatrists">Psychiatrists</option>
                <option value="Oncologists">Oncologists</option>
                <option value="Radiologists">Radiologists</option>
                <option value="Rheumatologists">Rheumatologists</option>

            </select> <br><br>

            <label>First Name: </label><br>
            <input class="first-name" type="text" name="f-name" placeholder="First Name" id="firstname"><br><br>

            <label>Last Name: </label><br>
            <input class="last-name"type="text" name="l-name" placeholder="Last Name" id="lastname"><br><br>

            <label>Email: </label><br>
            <input class="email" type="email" name="email" placeholder="Email" id="email"><br><br>

            <label>Phone number: </label><br>
            <input class="phoneno" type="tel" name="number" placeholder="Phone Number" id="phone"><br><br>

            <label>Password: </label><br>
            <input class="password" type="password" name="password" placeholder="Password" id="password"><br><br>

            <label>Gender: </label>
            <input class="gender" name="gender" type="radio" id="gender" value="Male"><span id="gender"> Male</span>
            <input type="radio"  name="gender" id="gender" value="Female"><span id="gender"> Female</span>
            <input type="radio" name="gender" id="gender" value="Others"><span id="gender"> Others</span><br><br>

            <input type="checkbox" id="ch"><span id="ch"> I Agree all the terms and conditions</span><br><br>

            <div class="text-center">
                <button class="btn btn-primary" type="submit" id="submit" name="submit">Register</button>
            </div>
        </form>
    </div>
</div>

<?php
include "../../database/doctor_service.php";
include "../../database/databaseService.php";


if(isset($_POST['submit']))
    insertDoc($_POST["bmdc"], $_POST["specialization"], $_POST["f-name"], $_POST["l-name"], $_POST["email"],$_POST["number"], $_POST["password"], $_POST["gender"]);
?>

</body>
</html>